
	// 1. Create variables to store to the following user details:
	
	let	myName = 'John';
	let mylastName = 'Smith';
	let myAge = 30;
	let currentHobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
	let workAddress = {
		houseNumber: 32,
		street: 'Washington',
		city: 'Lincoln',
		state: 'Nebraska'
	};


	console.log("First Name: " + myName);
	console.log("Last Name: " +mylastName);
	console.log("Age: " + myAge);
	console.log(currentHobbies);
	console.log(workAddress);

	/*Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:*/

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bfName = "Bucky Barnes";
	console.log("My bestfriend is: " + bfName);

	const lastLocation = "Arctic Ocean";
	const presentLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
